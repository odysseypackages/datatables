@section('datatables-css')
    <link rel="stylesheet" type="text/css" href="/vendor/DataTables/dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="/vendor/DataTables/src/css/dataTables.bootstrap.min.css"/>
@endsection

@include('datatables::partials/body',[$dataTable])

@section('datatables-js')
    <script type="text/javascript" src="/vendor/DataTables/datatables.min.js"></script>
    <script type="text/javascript" src="/vendor/DataTables/dataTables.responsive.min.js"></script>
@endsection

@if($dataTable->noSection)
    @include('datatables::partials/script',[$dataTable])
@else
    @push('datatables-js')
        @include('datatables::partials/script',[$dataTable])
    @endpush
@endif