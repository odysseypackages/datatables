<script>
    $(document).ready(function () {
        @if($dataTable->generate)
                {{$dataTable->generateName()}}();
        @endif
    });

    function {{$dataTable->generateName()}}() {
        $("#{{$dataTable->name()}}").dataTable({
            @include('datatables::partials/layout')
                    @foreach($dataTable->options() as $name => $option)
                    @if($name == 'order')
                    @php($order = explode(',',$option))
            'order': [[{{$order[0]}}, '{{$order[1]}}']],
            @else
            '{{$name}}': '{{$option}}',
            @endif
                    @endforeach
            "ajax": {
                "url": "{{route($dataTable->route(),$dataTable->params('route'))}}",
                "type": "GET",
                @if($dataTable->params())
                "data": {
                    @foreach($dataTable->params() as $key => $param)
                    "{{$key}}": {!! $param !!}@if(!$loop->last),@endif
                    @endforeach
                }
                @endif
            },
            "columns": [
                    @foreach($dataTable->columns() as $column)
                {
                    'name': '{{$column->get('field')}}',
                    @foreach($column->get('options') as $name => $value)
                            @if($name == 'orderable' || $name == 'searchable')
                    '{{$name}}': {{$value}},
                    @else
                    '{{$name}}': '{{$value}}',
                    @endif
                    @endforeach
                },
                    @endforeach
            ]
        });
    }
</script>
