# DataTables

Package for creating dataTables easily

## Install

`php artisan vendor:publish --tag=datatables`

## Basic usage

In controller use:

`Odysseycrew\DataTables\DataTable as DataTable`

create method for your table:

```
private function getTable()
{
 $table = new DataTable('tableOrModelName');
 return $table;
}
```

and use it

```
public function index()
{
    return response()->view('data.index',['dataTable' => $this->getTable()]);
}

public function indexAjax(Request $request)
{
    $output = $this->getTable()->output($request);
    return response()->json($output);
}
```
By default, *$table* will try get output from *$tableOrModelName.indexAjax*


finally, in view use:

`@dataTable($dataTable)`

##

If you want to include datatables JS an CSS files, use _'datatables-js'_ and _'datatables-css'_
sections in your view. 

**WARNING!**
If you don't want to use sections mentioned above, you need set parameter
_noSection_ to _true_ of your table;

Example:

```
private function getTable()
{
 $table = new DataTable('tableOrModelName');
 $table->noSection = true;
 return $table;
}
```


## Advanced usage

**__construct()**

`$table = new DataTable('tableOrModelName','routeName','columns','options'')

Construct accepts four parameters:
* model (model or table name)
* route (nullable, route name which return table output - default is 
$model.indexjax)
* columns (nullable, array of columns in format *['columnName','columnDatabaseField,'columnsOptions']*
)
* options (nullable, array in format *['option1' => '1', 'option2' => '2']*)

All this table parameters can be set by setter functions **setModel(), setRoute(),
setColumns()** and **setOptions()**

**addColumn()**

`$table->addColumns(['columnName','columnDatabaseField,'columnsOptionsArray'])`

**changeColumn()**

`$table->addColumns('columnDatabaseField',['columnName','columnDatabaseField,'columnsOptionsArray'])`

**removeColumns()**

Removes column by column field

`$table->removeColumns('columnsField')`

**setQuery()**

Sets data to output

Example:

```
$query = User::where('active',1)->get();
$table->setQuery($query);
```

**addToQuery()**

Adds rule to default query

Example:

```
$table->addToQuery(['where','active',1]);
```

**setActions()**

Adds actions at the end of the table. 
This method accepts array of actions in format *['actionLabel','actionName','content']*.
Parameters between *#* tag will be
replaced with model properties.

Example:

```
$table->setActions([
      ['Show','actions','<a href="'.route('user.show',['id' => '#id#']).'">Show</a>']
  ]);
}
```

In this example *#id#* will be replaced with *$user->id*

**setParams()**

Sets additional params to ajax call or route

Ajax:

``$table->setParams(['name' => 'Example'])``

Route:

``$table->setParams(['name' => 'Example'],'route')``

**addParams()**

Use it like _setParams()_ function. Params passed will be added to this previously defined

