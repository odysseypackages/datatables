<?php

namespace Odysseycrew\DataTables;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class DataTableServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([__DIR__.'/../resources/public_share' => public_path(),], 'datatables');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'datatables');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        Blade::directive('dataTable', function ($table) {
            return "<?php echo ($table)->draw(); ?>";
        });
    }
}
