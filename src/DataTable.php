<?php

namespace Odysseycrew\DataTables;

use Odysseycrew\DataTables\Services\DataTableService;

class DataTable
{
    protected $service;

    protected $model;

    protected $route;

    protected $columns;

    protected $options;

    protected $query;

    protected $queryChanges;

    protected $routeParams = [];

    protected $ajaxParams = [];

    public $noSection = false;

    public $generate = true;

    public function __construct($model, $route = null, $columns = [], $options = [])
    {
        $this->service = new DataTableService($model);
        $this->model = $model;
        $this->route = $this->service->createRoute($route);
        $this->columns = $this->service->createColumns($columns);
        $this->options = $options;
    }

    public function model()
    {
        return $this->model;
    }

    public function route()
    {
        return $this->route;
    }

    public function columns()
    {
        return $this->columns;
    }

    public function options()
    {
        return $this->options;
    }

    public function params($type = 'ajax'){
        if($type == 'ajax'){
           return $this->ajaxParams;
        }else if($type == 'route'){
            return $this->routeParams;
        }else{
            return [];
        }
    }

    public function query($request = null)
    {
        if(!$this->query && $request){
            $this->query = $this->service->createQuery($request, $this->columns);
        }
        return $this->query;
    }

    public function queryChanges()
    {
        return $this->queryChanges;
    }

    public function setRoute($route)
    {
        $this->route = $this->service->createRoute($route);
        return $this;
    }

    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    public function setColumns($columns)
    {
        $this->columns = $this->service->createColumns($columns);
        return $this;
    }

    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    public function setQuery($query){
        $this->query = $query;
        return $this;
    }

    public function setTable($table){
        $this->service->setTable($table);
        return $this;
    }

    public function setParams($params, $type = 'ajax'){
        if($type == 'ajax'){
            $this->ajaxParams = $params;
        }else if($type == 'route'){
            $this->routeParams = $params;
        }

        return $this;
    }

    public function addParams($params, $type = 'ajax'){
        if($type == 'ajax'){
            $this->ajaxParams = array_merge($this->ajaxParams, $params);
        }else if($type == 'route'){
            $this->routeParams = array_merge($this->routeParams, $params);
        }

        return $this;
    }

    public function addColumn($column){
        $column = $this->service->createColumn($column);
        $this->columns->push($column);
    }

    public function removeColumn($field){
        $key = $this->columns->search(function($item) use ($field) {
            return $item->get('field') == $field;
        });

        $this->columns->pull($key);
    }

    public function changeColumn($field, $column){
        $key = $this->columns->search(function($item) use ($field) {
            return $item->get('field') == $field;
        });

        $this->columns->get($key)->set($column);
    }

    public function addToQuery(...$elements){
        foreach ($elements as $element){
            $this->queryChanges[] = $element;
        }
    }

    //pobiera wartość z atrybutu 'options'
    public function get($attribute, $isArray = false){
        if(key_exists($attribute, $this->options)){
            return $this->options[$attribute];
        }
        if($isArray){
            return [];
        }
        return false;
    }

    public function name(){
        return $this->service->getTableName($this->options);
    }

    public function generateName(){
        return 'generate'.ucfirst($this->service->getTableName($this->options));
    }

    public function draw(){
        return view('datatables::datatable',['dataTable' => $this]);
    }

    public function cssClass(){
        if(key_exists('classes', $this->options)){
            return $this->options['classes'];
        }else{
            return 'table';
        }
    }

    public function output($request){
        if(!$this->query){
            $this->query = $this->service->createQuery($request, $this);
        }
        return $this->service->createOutput($request, $this);
    }

}
