<?php

namespace Odysseycrew\DataTables\Services;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Exception;

class DataTableService
{
    protected $columnRemove = ['created_at', 'updated_at', 'deleted_at'];

    protected $model;

    protected $table;

    public function setTable($table){
        $this->table = $table;
        return $this;
    }

    public function __construct($name)
    {
        if (class_exists($name)) {
            $this->model = $name;
            $this->table = with(new $name())->getTable();
        } else {
            if(!Schema::hasTable($name)) {
                throw new Exception('Table or Model "'.$name.'" not found');
            }
            $class = 'App\\'.ucfirst(substr($name, 0, -1));
            if (class_exists($class)) {
                $this->model = $class;
            }
            $this->table = $name;
        }
    }

    public function createRoute($route)
    {

        if (!$route) {
            if ($this->model) {
                $prefix = strtolower(substr($this->model, strrpos($this->model, '\\') + 1));
            } else {
                $prefix = $this->table;
            }
            $newRoute = $prefix . '.indexAjax';
        } else {
            $newRoute = $route;
        }
        return $newRoute;
    }

    public function createColumns($columns)
    {
        $newColumns = collect([]);
        if (is_array($columns)) {
            if (empty($columns)) {
                $newColumns = $this->createColumnsFrom();
            } else {
                foreach ($columns as $column) {
                    $newColumns->push($this->createColumn($column));
                }
            }
        } else if ($columns instanceof Collection) {
            if ($columns->isEmpty()) {
                $newColumns = $this->createColumnsFrom();
            } else {
                foreach ($columns as $column) {
                    $newColumns->push($this->createColumn($column));
                }
            }
        }
        return $newColumns;
    }

    public function createColumn($column)
    {
        if (is_array($column)) {
            $newColumn = new DataTableColumn();
            $newColumn->set($column);
            return $newColumn;
        } else if ($column instanceof DataTableColumn) {
            return $column;
        }
        return null;
    }

    public function createQuery($request, $table)
    {
        $pageNum = $request['start'] / $request['length'] + 1;

        if ($this->model) {
            $query = $this->model::orderBy($table->columns()->get($request['order'][0]['column'])->get('field',0), $request['order'][0]['dir']);
        } else {
            $query = DB::table($this->table)->orderBy($table->columns()->get($request['order'][0]['column'])->get('field',0), $request['order'][0]['dir']);
        }
        if (!empty($table->queryChanges())) {
            foreach ($table->queryChanges() as $queryChange) {
                $newQueryChange = $queryChange;
                unset($newQueryChange[0]);
                $query = call_user_func_array([$query, $queryChange[0]], $newQueryChange);
            }
        }
        $query = $query->paginate($request['length'], ['*'], 'page', $pageNum);

        return $query;
    }

    public function createOutput($request, $table)
    {

        $output = [
            "draw" => $request->get('draw'),
            "recordsTotal" => $table->query()->total(),
            "recordsFiltered" => $table->query()->total(),
            "aaData" => [],
            "collection" => $table->query()
        ];
        return $output;
    }

    public function getTableName($options)
    {
        if (key_exists('name', $options)) {
            return $options['name'];
        } else {
            if ($this->model) {
                return strtolower(substr($this->model, strrpos($this->model, '\\') + 1)) . 'Table';
            } else {
                return strtolower($this->table) . 'Table';
            }
        }
    }

    private function createColumnsFrom()
    {
        $newColumns = collect([]);
        if ($this->model) {
            $table = with(new $this->model)->getTable();
            $columns = Schema::getColumnListing($table);
        } else {
            $columns = Schema::getColumnListing($this->table);
        }
        foreach ($this->columnRemove as $cr) {
            if (($key = array_search($cr, $columns)) !== false) {
                unset($columns[$key]);
            }
        }
        foreach ($columns as $column) {
            $newColumns->push($this->createColumn([ucfirst($column), $column]));
        }
        return $newColumns;
    }
}
